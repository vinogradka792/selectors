var selectorFindBtn = document.querySelector(".selector-find");
var selectorNextBtn = document.getElementById("btn-next");
var selectorPrevBtn = document.getElementById("btn-prev");
var selectorNextSiblingBtn = document.querySelector(".nav-right");
var selectorPrevSiblingBtn = document.querySelector(".nav-left");
var selectorParentBtn = document.querySelector(".nav-top");
var selectorChildBtn = document.querySelector(".nav-bottom");

function enteredElements(items) {
    var index = 0;
    var clickBtns = (id, plusMinus) => document.getElementById(id)

        .addEventListener('click',() => {
            document.querySelector(".active").classList.remove('active');
            updateClass(plusMinus)

        });

    clickBtns('btn-prev', -1);
    clickBtns('btn-next', 1);

    function updateClass(upd) {
        index = (index + upd + items.length) % items.length;
        for (var i = 0; i < items.length; i++) {
            items[i].classList[i === index ? 'add' : 'remove']('active');
        }
    }
}
selectorFindBtn.addEventListener('click',() => {
    var selectorInp = document.querySelector(".selector").value;

    if(/^[0-9]+$/.test(selectorInp) === true || selectorInp.length === 0 || document.querySelectorAll(selectorInp).length === 0){
        alert("Такого нету!")
        selectorNextBtn.setAttribute("disabled", "disabled");
        selectorPrevBtn.setAttribute("disabled", "disabled");
        selectorNextSiblingBtn.setAttribute("disabled", "disabled");
        selectorPrevSiblingBtn.setAttribute("disabled", "disabled");
        selectorParentBtn.setAttribute("disabled", "disabled");
        selectorChildBtn.setAttribute("disabled", "disabled");
    }
    else{
        if(document.querySelector('.active') !== null){
            document.querySelector('.active').classList.remove("active");
        }
        if(selectorInp[0] !== "#" && selectorInp[0] !== "."){
            selectorNextBtn.removeAttribute("disabled");
            selectorPrevBtn.removeAttribute("disabled");
            var selectActiveItem = document.querySelectorAll(selectorInp);
            selectActiveItem[0].classList.add("active")
            enteredElements(selectActiveItem);
        }
        else{
            var selectNonActiveItem = document.querySelectorAll(selectorInp);
            selectNonActiveItem[0].classList.add("active")
            enteredElements(selectNonActiveItem);
            selectorNextBtn.setAttribute("disabled", "disabled");
            selectorPrevBtn.setAttribute("disabled", "disabled");
        }

        if(document.querySelector(".active").parentElement !== null){
            selectorParentBtn.removeAttribute("disabled");
        }
        selectorParentBtn.addEventListener('click',() => {
            if(document.querySelector(".active").parentElement !== null){
                selectorNextBtn.setAttribute("disabled", "disabled");
                selectorPrevBtn.setAttribute("disabled", "disabled");
                var activeEl = document.querySelector(".active");
                var activeParentEl = activeEl.parentElement;
                activeParentEl.classList.add('active');
                activeEl.classList.remove('active');
            }
            else{
                selectorParentBtn.setAttribute("disabled", "disabled");
            }
        });

        if(document.querySelector(".active").children[0] !== null){
            selectorChildBtn.removeAttribute("disabled");
        }
        selectorChildBtn.addEventListener('click',() => {
            for (let i = 0; i < document.querySelector(".active").children.length; i++) {
                var activeChildEl = document.querySelector(".active").children[0];
            }
            if(activeChildEl !== undefined) {
                selectorNextBtn.setAttribute("disabled", "disabled");
                selectorPrevBtn.setAttribute("disabled", "disabled");
                var activeEl = document.querySelector(".active");
                activeEl.classList.remove('active');
                for (let i = 0; i < activeEl.children.length; i++) {
                    activeChildEl.classList.add('active');
                    activeEl.classList.remove('active');
                }
            }
            else{
                selectorChildBtn.setAttribute("disabled", "disabled");
            }
        });

        if(document.querySelector(".active").previousElementSibling !== null) {
            selectorPrevSiblingBtn.removeAttribute("disabled");
        }
        selectorPrevSiblingBtn.addEventListener('click', () => {
            if(document.querySelector(".active").previousElementSibling !== null) {
                selectorNextBtn.setAttribute("disabled", "disabled");
                selectorPrevBtn.setAttribute("disabled", "disabled");
                var activeEl = document.querySelector(".active");

                var activeNextEl = activeEl.previousElementSibling;
                activeEl.classList.remove('active');
                activeNextEl.classList.add('active');
            }
            else{
                selectorPrevSiblingBtn.setAttribute("disabled", "disabled");
            }
        });

        if(document.querySelector(".active").nextElementSibling !== null) {
            selectorNextSiblingBtn.removeAttribute("disabled");
        }
        selectorNextSiblingBtn.addEventListener('click', () => {
            if(document.querySelector(".active").nextElementSibling !== null) {
                selectorNextBtn.setAttribute("disabled", "disabled");
                selectorPrevBtn.setAttribute("disabled", "disabled");
                var activeEl = document.querySelector(".active");
                var activeNextEl = activeEl.nextElementSibling;
                activeEl.classList.remove('active');
                activeNextEl.classList.add('active');
            }
            else{
                selectorNextSiblingBtn.setAttribute("disabled", "disabled");
            }
        });
    }
});
